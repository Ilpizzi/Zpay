<section>
    <div class="container">
        <div class="row">
            <div style="font-family:'Roboto', sans-serif;height: 100vh;width: 100vw; display: flex; justify-content: center; align-items: center;">
                <div style="display:flex; flex-direction: column; align-items: center">
                    <img style="height:130px " src="/images/zpay-logo.svg" alt="ZPAY LOGO">
                    <div style="font-size: 10px; text-align: center;line-height:1.5; font-style:italic; margin-top: 20px">
                        <p>
                            ZPAY SRL - via Solferino 1 - LODI (LO) - Cod. Fisc. e Part IVA n° 11865370966
                            <br>
                            C.C.I.A.A di Lodi, REA n°LO-2671136
                            <br>
                            Capitale Sociale € 1.500.000,00 euro i.v.
                            <br>
                            Codice Identificativo dell’Istituto di Pagamento n: 18175
                            <br>
                            Società con socio unico
                        </p>
                        <p style="font-style: normal; font-size: 12px; color: #0f68bd ">
                            <strong>email: info@zpay.it - pec: zpay@gruppozucchetti.it - tel: +39 0371 594-1</strong>
                        </p>
                    </div>

                </div>
            </div>
        </div>

    </div>
</section>
