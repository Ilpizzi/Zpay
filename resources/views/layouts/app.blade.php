<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.bunny.net/css?family=Nunito" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">

    <!-- Scripts -->
    @vite(['resources/js/app.js'])
</head>
<body>
    <header>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="header-content">
                        <a href="/" rel="home">
                            <img src="/images/zpay-logo.svg" alt="Zpay">
                        </a>
                        <nav class="desktop--menu">
                            <ul class="desktop-menu-ul">
                                <li class="desktop-menu-li" data-id="how-it-works">
                                    <a class="desktop-menu-link"  href="">Fatture</a>
                                </li>
                                <li class="desktop-menu-li" data-id="how-it-works">
                                    <a class="desktop-menu-link"  href="">Conti e Carte</a>
                                </li>
                                <li class="desktop-menu-li" data-id="how-it-works">
                                    <a class="desktop-menu-link"  href="">Contatti</a>
                                </li>
                                <li  class="desktop-menu-li d-flex align-items-center" data-id="how-it-works">
                                    <button class="desktop-menu-link"  href="">INIZIA SUBITO</button>
                                </li>
                                {{--<li class="desktop-menu-li" data-id="make-contract">
                                    <a class="desktop-menu-link"  href="">Crea contratto</a>
                                    <div class="dropdown-menu-links" id="make-contract">
                                        <a class="text-dropdown" href="">Contratto 4+4</a>
                                        <a class="text-dropdown" href="">Contratto ad uso transitorio</a>
                                        <a class="text-dropdown" href="">Contratto per studenti</a>
                                        <a class="text-dropdown" href="">Modelli di Contratto</a>
                                    </div>
                                </li>--}}
                            </ul>
                        </nav>
                        <div class="hamburger--menu d-none">
                            <i class="far fa-bars"></i>
                            <i class="far fa-times"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div id="app">
        <main class="">
            @yield('content')
        </main>
    </div>
    <footer class="">
        <div class="container">
            <div class="row">
                <div class="col-xl-12  col-lg-10 col-md-12 col-12 p-0">
                    <div class="row">
                        <div class="col-lg-2 col-md-6 col-12">
                            <div class="footer--column d-flex align-items-center">
                                <a href="https://www.zucchetti.it/website/cms/home.html"><img src="/images/zucchetti-logo.png" alt="Domeo" class="mb-small"></a>
                            </div>
                        </div>
                        <div class=" d-flex col-lg-7 col-md-6 col-12">
                            <div class="col-3 footer--column mb-regular">
                                <h6 class="mb-small">Gruppo</h6>
                                <a href="">Chi siamo</a>
                                <br>
                                <a href="">Contatti</a>
                            </div>
                            <div class="col-3 footer--column mb-regular">
                                <h6 class="mb-small">LAVORA CON NOI</h6>
                                <a href="">Selezioni in corso</a>
                            </div>
                            <div class="col-3 footer--column mb-regular">
                                <h6 class="mb-small">Eventi</h6>
                            </div>
                            <div class="col-3 footer--column mb-regular">
                                <h6 class="mb-small">Ufficio Stampa</h6>
                                <a href="">Comunicati</a>
                                <br>
                                <a href="">Rassegna</a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-12">
                            <div class="footer--column d-flex align-items-center">
                                <img src="/images/zucchetti-logo.png" alt="" class="mb-small"><br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <section class="section--content section--content-copyright py-regular">
            <div class="container">
                <div class="row">
                    <div class="col-xl-8 offset-xl-2 col-lg-10 offset-lg-1 col-md-12 col-12 text-center">
                        <h6 class="additional-text"></h6>
                    </div>
                </div>
            </div>
        </section>
    </footer>
</body>
</html>
