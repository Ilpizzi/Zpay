@extends('layouts.app')

@section('content')
<section class="content-wrapper-with-bg">
    <div class="container">
        <div class="row">
            <div class="col-12 p-0">
                <div class="text-center mt--large">
                    <h1 class="color-primary mb--medium">RICONCILIAZIONE CONTI </h1>
                    <p class="mb--medium">Iscriviti subito a ZPAY e collega tutti tuoi conti correnti per poterli gestire da un' unica dashboard.</p>
                    <button class="btn-zpay-dark"  href="">INIZIA SUBITO</button>
                </div>
                <div class="d-flex justify-content-center">
                    <img src="/images/data-img.svg" alt="">
                </div>
            </div>
        </div>
    </div>
</section>
<section class="content-white">
    <div class="container">
        <div class="row">
            <div class="col-12 d-flex">
                <div class="col-6">
                    <div class="title-header mb-2">OPEN BANKING</div>
                    <h1 class="color-primary">RICONCILIAZIONE <br> CONTI CORRENTI</h1>
                </div>
                <div class="col-6">
                    <p>ZPAY permette di gestire tutti i tuoi conti correnti in unico pannello, dalla quale potrai così visionare i movimenti e gestire i pagamenti. </p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="content-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-12 p-0">
                <div class="col-12 d-flex">
                    <div class="col-6 p-0 m-0">
                        <img style="width: 100%" src="/images/calculator.png" alt="">
                    </div>
                    <div class=" col-6 d-flex flex-column align-items-center justify-content-center">
                        <div style="padding-left:50px">
                            <div class="color-primary title-header mb-2">SEZIONE PER SPIEGARE</div>
                            <h3 class="">Lorem ipsum dolor sit amet, consectetur adipiscing elit</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                            <button class="btn-zpay-light"  href="">Scopri di più</button>
                        </div>
                    </div>
                </div>
                <div class="col-12 d-flex">
                    <div class="col-6 d-flex flex-column align-items-center justify-content-center p-0">
                        <div style="padding-right:50px">
                            <div class="color-primary title-header mb-2">SEZIONE PER SPIEGARE</div>
                            <h3 class="">Lorem ipsum dolor sit amet, consectetur adipiscing elit</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                            <button class="btn-zpay-light"  href="">Scopri di più</button>
                        </div>
                    </div>
                    <div class="col-6 p-0">
                        <img style="width: 100%" src="/images/pos.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="content-white">
    <div class="container">
        <div class="row">
            <div class="col-12 ">
                <div class="col-12 text-center">
                    <div class="title-header mb-2">VANTAGGI</div>
                    <h1 class="color-primary">Lorem ipsum dolor sit amet, consectetur adipiscing elit</h1>
                </div>
                <div class="col-12 mt--medium">
                    <div class="col-12 d-flex justify-content-center">
                        <div class="col-4 d-flex align-items-center">
                            <div class="col-2">
                                <div class="icon-home"><img src="/images/file.svg" alt=""></div>
                            </div>
                            <div class="col-10 d-flex align-items-center p-2">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                            </div>
                        </div>
                        <div class="col-4 d-flex align-items-center">
                            <div class="col-2">
                                <div class="icon-home"><img src="/images/bank-transfer.svg" alt=""></div>
                            </div>
                            <div class="col-10 d-flex align-items-center p-2">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                            </div>
                        </div>
                        <div class="col-4 d-flex align-items-center">
                            <div class="col-2">
                                <div class="icon-home"><img src="/images/credit-card.svg" alt=""></div>
                            </div>
                            <div class="col-10 d-flex align-items-center p-2">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                            </div>
                        </div>
                    </div>
                    <div class="col-12 d-flex justify-content-center mt--medium">
                        <div class="col-4 d-flex align-items-center">
                            <div class="col-2">
                                <div class="icon-home"><img src="/images/pen-tool.svg" alt=""></div>
                            </div>
                            <div class="col-10 d-flex align-items-center p-2">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                            </div>
                        </div>
                        <div class="col-4 d-flex align-items-center">
                            <div class="col-2">
                                <div class="icon-home"><img src="/images/map-bank.svg" alt=""></div>
                            </div>
                            <div class="col-10 d-flex align-items-center p-2">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                            </div>
                        </div>
                        <div class="col-4 d-flex align-items-center">
                            <div class="col-2">
                                <div class="icon-home"><img src="/images/smartphone.svg" alt=""></div>
                            </div>
                            <div class="col-10 d-flex align-items-center p-2">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="content-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-12 p-0">
                <div class="text-center mt--large">
                    <h1 class="color-primary mb--medium">LOREM iPSuM DOLOR SIT AMET</h1>
                    <p class="mb--medium">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                </div>
                <div class="d-flex justify-content-center align-items-center mb--medium">
                    <div class="vertical-slider-img col-6" style="">
                    </div>
                    <div  class="vertical-slider col-5">
                        <div id="vertical-slider-first">
                            <div id="vertical-slider-collapsed-first" class="vertical-slider-title  d-flex align-items-center d-none"><h4>Carta di Credito</h4></div>
                            <div id="vertical-slider-expanded-first" class="vertical-slider-content active d-flex flex-column justify-content-center">
                                <div class="color-primary title-header mb-2">Blocco Fatturazione</div>
                                <h3 class="">Carta di Credito</h3>
                                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita</p>
                            </div>
                        </div>
                        <div class="mt-4">
                            <div id="vertical-slider-collapsed-first" class="vertical-slider-title d-flex align-items-center"><h4>Attraverso <br> Bonifico</h4></div>
                            <div id="vertical-slider-expanded-first" class="vertical-slider-content d-none ">
                                <div class="color-primary title-header mb-2">Blocco Fatturazione</div>
                                <h3 class="">Attraverso Bonifico</h3>
                                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita</p>
                            </div>
                        </div>
                        <div class="mt-4">
                            <div id="vertical-slider-collapsed-first" class="vertical-slider-title d-flex align-items-center"><h4>Tramite <br> Sdd</h4></div>
                            <div id="vertical-slider-expanded-first" class="vertical-slider-content d-none">
                                <div class="color-primary title-header mb-2">Blocco Fatturazione</div>
                                <h3 class="">Tramite Sdd</h3>
                                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-center">
                    <button class="btn-zpay-dark"  href="">INIZIA SUBITO</button>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
