<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class GetBankImages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get-bank:logo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Recupero loghi banche e salvataggio nella cartella banche in public';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $banksList = json_decode(file_get_contents(storage_path('app/bankLists.json')));

        foreach($banksList->providers as $provider){
            if(!str_contains($provider->logo, "default")){
                $logo = file_get_contents("https://dashboard.tspay.app/assets/images/banks/".$provider->logo);
                $extension = explode('.', $provider->logo)[1];

                file_put_contents(public_path('banche/'.$provider->logo), $logo);
            }

        }


        return Command::SUCCESS;
    }
}
