<?php

namespace App\Console\Commands;

use App\Models\BankProvider;
use App\Models\BankProviderProduct;
use Illuminate\Console\Command;

class ImportCBIBankList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cbi-banks:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Imports All CBI Banks List in DB';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $banksList = json_decode(file_get_contents(storage_path('app/bankListCBI.json')));
        $banksImages = json_decode(file_get_contents(storage_path('app/bankLists.json')));


        foreach($banksList as $provider){
            $logo = null;
            $order = 1000;
            foreach($banksImages->providers as $image){
              if( $image->id == $provider->id){
                  if($image->logo ==='default_logo.png'){
                      $logo = 'placeholder_bank_logo.png';
                  }else{
                      $logo = $image->logo;
                  }

                  $order = $image->order;
              }

            }


            $bank = BankProvider::create([
                'aspsp_code' => $provider->aspspCode,
                'aspsp_id' => $provider->id,
                'consent_on_specified_accounts' => $provider->aisInfo->consent_on_specified_accounts,
                'contact_email' => $provider->aisInfo->aspspContactMail ?? null,
                'country_code' => $provider->countryCode,
                'global_consent' => $provider->aisInfo->globalBankConsent,
                'is_active' => true,
                'logo' => $logo,
                'max_fetch_interval_for_transactions' => $provider->aisInfo->maxFetchIntervalForTransactions,
                'name' => $provider->businessName,
                'position_order' => $order,
                'supported_natures' => $provider->aisInfo->supportedNatures,
                'vat_number' => $provider->vatCode,
                'psd2_provider_id' => 2,
            ]);

            foreach($provider->aisInfo->aspspProductsList as $product){
                $bankProduct = BankProviderProduct::create([
                    'bank_provider_id' => $bank->id,
                    'aspsp_product_code' => $product->aspspProductCode,
                    'aspsp_product_label' => $product->aspspProductSuggestedLabel,
                    'aspsp_product_description' => $product->aspspProductDescription,
                ]);
            }
        }


        return Command::SUCCESS;
    }
}




