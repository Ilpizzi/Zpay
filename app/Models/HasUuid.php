<?php


namespace App\Models;

use Illuminate\Support\Str;

trait HasUuid
{
    public static function bootHasUuid()
    {
        static::creating(function ($user) {
            $user->uuid = Str::uuid()->getBytes();
        });
    }

    protected function getArrayableItems(array $values)
    {
        if (! in_array('id', $this->hidden)) {
            // $this->hidden[] = 'id';
        }

        return parent::getArrayableItems($values);
    }

    public function getRouteKeyName()
    {
        return 'uuid';
    }
}
