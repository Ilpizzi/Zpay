<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BankProvider extends Model
{
    use HasFactory;
    use HasUuid;

    protected $guarded = ['id'];
    public $timestamps = false;
}
