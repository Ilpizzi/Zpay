<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BankProviderProduct extends Model
{
    use HasFactory;
    use HasUuid;

    protected $guarded = ['id'];
    public $timestamps = false;
}
